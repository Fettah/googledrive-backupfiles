package server_client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.StringTokenizer;


public class theServer {

	private ServerSocket googleServer;
	private Socket socket;
	private StringTokenizer strtok;
	private InputStream in;
	private OutputStream out;
	private BufferedReader commandReader;
	private DataInputStream fileReader;
	private DataOutputStream fileWriter;
	private PrintStream streamWriter;
	private String path;
	private String username;
	private File[] ServerFiles;
	private String Command;
	private String[] files;
	private FileInputStream fileInStream;
	private String receivedLastDatModifed;
	private int index;

	public void run() throws UnknownHostException, IOException, InterruptedException {

		initilize();

		// this will read "sing in" or "sing up"
		Command = commandReader.readLine();
		strtok = new StringTokenizer(Command, " ");

		if (strtok.nextToken().equals("SIGNIN")) {

			this.username = strtok.nextToken();
			String password = null;
			password = strtok.nextToken();

			if (username.equals("celine") && password.equals("dion")) {
				streamWriter.println("SUCCESSFUL");
				prepareServerFiles();
				files = new String[ServerFiles.length];
				
				files = filesToString(files);// return only the name of the file
				System.out.println("****************************************");
				System.out.println("SERVER FILES: " + Arrays.toString(files));
				System.out.println("****************************************");
				
				while(this.index <= files.length) {
					System.out.println("INDEX: " + index + ", FILES LENGTH: " +files.length);	
					if(index == files.length) {//Checked all the files
						
						//Thread.sleep(10000);//this will be removed later for sure[Big file to transfer
											//=> more time to wait => the other may not be copied ]
//						System.out.println("----------------------------------------");
//						System.out.println("WOKE UP FROM SLEEPING");
//						System.out.println("----------------------------------------");
						
						this.index = 0;
					}
					sync();
					index++;//fetch the next file
				}
				
			} else {
				// invalid username or password
				streamWriter.println("UNSUCCESSFUL");
			}

		} else if (Command.equals("SIGNUP")) {
			// to be implemented later
		}
	}

	private void initilize() throws IOException {

		this.googleServer = new ServerSocket(5555);
		System.out.println("Server is waiting on port 5555");
		this.socket = googleServer.accept();

		this.in = socket.getInputStream();
		this.out = socket.getOutputStream();

		this.commandReader = new BufferedReader(new InputStreamReader(in));
		this.streamWriter = new PrintStream(socket.getOutputStream());
		this.path = getClass().getClassLoader().getResource(".").getPath();
		this.index = 0;//index of the array that has the files to be checket by the user

	}

	/*
	 * Get the file names from the server and store them in the "files" variable
	 * Support only one folder so far
	 */

	private void prepareServerFiles() {

		String path = "D:\\Eclipse Projects\\ITHWLearn\\server_files\\"+ this.username;
		File folder = new File(path);
		this.ServerFiles = folder.listFiles();
	}

	private String[] filesToString(String[] files) {
		int cnt = 0;
		int lastIndex;
		String tmp;

		while (cnt < ServerFiles.length) {
			lastIndex = ServerFiles[cnt].toString().lastIndexOf("\\");
			tmp = ServerFiles[cnt].toString().substring(lastIndex + 1);
			files[cnt++] = tmp;
		}
		return files;
	}

	private void sync() throws IOException {

		// test for one file now
		
		int fileSize;
		
		String fileName = files[index].toString();
		String fileDate = Long.toString(ServerFiles[index].lastModified());
		/*
		 * =>Protocol Server send "?_FILENAME_FILEDATE"
		 */
		System.out.println("SYNCHRONIZING: " + fileName);
		Command = "?_" + fileName + "_" + fileDate;
		System.out.println("IM SENDING THIS FILE TO THE USER: "+fileName);
		streamWriter.println(Command);
		Command = commandReader.readLine();// Read "Download", "Upload fileSize"
											// or "Ignore"
		System.out.println("READ THIS COMMAND: " + Command);
		strtok = new StringTokenizer(Command, " ");
		String upload = strtok.nextToken();

		if (Command.equalsIgnoreCase("Download")) {
			sendFileToClient(fileName);
		} else if (upload.equalsIgnoreCase("upload")) {
			//don't move this line from here, cause during the download there is not size in the token
			fileSize = Integer.parseInt(strtok.nextToken());
			this.receivedLastDatModifed = strtok.nextToken();
			System.out.println("LAST DATE MODIFIED GOT FROM THE CLIENT: "+ receivedLastDatModifed);
			getFileFromClient(fileName, fileSize);
		} else if (Command.equalsIgnoreCase("ignore")) {
			System.out.println(fileName+" IS UP TO DATE");
		}
		
		
		System.out.println("END OF SYNC");
		
	}// end of sync

	private void sendFileToClient(String file) throws IOException {
		
		System.out.println("-----------------");
		System.out.println("SENDING..........");
		path = "D:\\Eclipse Projects\\ITHWLearn\\server_files" + "\\"+ username + "\\" + file;
		byte[] bytes;
		
		fileInStream = new FileInputStream(path);
		System.out.println("FILE PATH: " + path);
		this.fileWriter = new DataOutputStream(out);
		long fileSize = fileInStream.available();

		/*
		 * =>Protocol send File Size to the client
		 */
		Command = "OK " + fileSize;
		streamWriter.println(Command);
		//streamWriter.flush();
		String ok = commandReader.readLine();
				if(ok.equalsIgnoreCase("ok")) {
					System.out.println("PERMISSION TO WRITE TO THE CLIENT");
				}
		bytes = new byte[(int) fileSize];
		fileInStream.read(bytes);
		fileWriter.write(bytes);
		System.out.println("FINISHED SENDING");
	}

	private void getFileFromClient(String file, int fileSize)
			throws NumberFormatException, IOException {
		
		System.out.println("-----------------");
		System.out.println("GETTING FILE: " + file);
		String path = "D:\\Eclipse Projects\\ITHWLearn\\server_files\\"	+ username + "\\" + file;
		FileOutputStream fileOutStream = new FileOutputStream(path);

		System.out.println("PATH TO STORE FILE: " + path);
		this.fileReader = new DataInputStream(in);
		System.out.println("FILE " + file + " GOTTEN");
		byte[] bytes = new byte[fileSize];
		//streamWriter.println("OK");
		streamWriter.println("OK");
		fileReader.readFully(bytes);
		System.out.println("READFYLLY EXECUTED");
		
		fileOutStream.write(bytes);
		System.out.println("BYTES WRITTEN");
		fileOutStream.close();
		System.out.println("FINISHED GETTING: " + file);
		new File(path).setLastModified(Long.parseLong(this.receivedLastDatModifed));
		
		System.out.println("-----------------------------------------------------");
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
		System.out.println("CHANGED DATE TO :" + sdf.format(new Date(Long.parseLong(receivedLastDatModifed))));
		System.out.println("CHANGED DATE TO :" + this.receivedLastDatModifed.toString());
		System.out.println("-----------------------------------------------------");
		

	}
}
