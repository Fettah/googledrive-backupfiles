package server_client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.apache.commons.lang3.ArrayUtils;


public class theClient {


	private ArrayList<String> userFiles;
	private ArrayList<String> userfilesdates;
	private String password;
	private String username;
	private File[] clientFiles;
	private Socket clientSocket;
	private InputStream in;
	private OutputStream out;
	private BufferedReader commandReader;
	private DataInputStream fileReader;
	private DataOutputStream fileWriter;
	private PrintStream streamWriter;
	private StringTokenizer strtok;
	private String Command;
	private String receivedFileName;
	private String receivedLastModified;
	private String lastDateModified;
	private String userFilesInStringArray[];
	private String userDateInStringArray[];
	private FileInputStream fileInStream;
	Scanner sc = new Scanner(System.in);
	String path = getClass().getClassLoader().getResource(".").getPath();
	

	public void run() throws UnknownHostException, IOException, ParseException {

		initilise();
		System.out.println("IM THE CLIENT");
		System.out.println("Are you a new user or a registered user?");
		System.out.println("press \"IN\" to sign in or \"UP\" to sign up: ");
		String sign = sc.next();
		
		// while(true)
		if (sign.equalsIgnoreCase("IN")) {
			// getPassAndUserName();
			Command = "SIGNIN " + username + " " + password;
			Command = "SIGNIN celine dion";
			// send the command: implementation 2 in the protocol
			streamWriter.println(Command);

		} else if (sign.equalsIgnoreCase("UP")) {
			getPassAndUserName();
			Command = "SIGNUP " + username + " " + password + " ";
			streamWriter.println(Command);

		} else {
			System.out.println("incorrect chooice, please retype again");
		}

		// should expected "successful to be sent"
		String successCommand = commandReader.readLine();

		// check password and username are correct
		if (checkSuccess(successCommand)) {
			System.out.println("CONNECTED TO THE SERVER");
			System.out.println("Checking files to be downloaded......");
			
			 while(true)
				 sync();
			

		} else {
			System.out.println("COULD NOT CONNECT TO THE SERVER");
			System.out.println("Check your username and password");
		}

	}// end of Run method

	private void initilise() throws UnknownHostException, IOException {

		this.clientSocket = new Socket("localhost", 5555);
		this.in = clientSocket.getInputStream();
		this.out = clientSocket.getOutputStream();
		// read command and files sent by the server
		this.commandReader = new BufferedReader(new InputStreamReader(in));
		//this.fileReader = new DataInputStream(in);
		// print stream is used to send commands and files
		this.streamWriter = new PrintStream(clientSocket.getOutputStream());
		this.Command = null;
	}

	private void getPassAndUserName() {
		System.out.println("Username: ");
		username = sc.next();
		System.out.println("Password");
		password = sc.next();
	}

	private boolean checkSuccess(String command) {
		if (command.equalsIgnoreCase("SUCCESSFUL")) {
			return true;
		} else
			return false;
	}
	
	private void sync() throws IOException, ParseException{
		
			String operation = null;//Download or Upload
			System.out.println("------------------\nSYNC");
			System.out.println("BEFORE IT CRASHS:");
			Command = commandReader.readLine();	
			System.out.println("COMMAND IS EXPECTED");
			strtok = new StringTokenizer(Command,"_");
			
			if(strtok.nextToken().equals("?")) {
				
				receivedFileName = strtok.nextToken();
				receivedLastModified = strtok.nextToken();
				//check what operation needs to be done on this file
				operation = checkThisFile(receivedFileName, receivedLastModified);	
				
				if(operation.equalsIgnoreCase("download")) {
					downloadFile(receivedFileName);
				}
				else if(operation.equalsIgnoreCase("upload")) {
					uploadFile(receivedFileName);//Send with it the last date modified				
				}
				else if (operation.equalsIgnoreCase("ignore")){
					//send me the next file
					streamWriter.println("ignore");
				}
				//else
					//something bad occurred
			}
	}//End of Sync
	
	private String checkThisFile(String serverFile, String serverDate) {
		
		prepareUserFiles();
		
		int index = ArrayUtils.indexOf(userFilesInStringArray, serverFile);
			if(index >= 0) {// file exist
				
				System.out.println("CHECK DATE FOR  :"+serverFile);
				//there may a problem in this function, it looks weird
				int operation = checkDates(userDateInStringArray[index], serverDate);
				System.out.println("OPERATION RETURNED: " + operation);
				
				if(operation == 1) 
					return "Download";
				else if(operation == -1)
					return "upload";
				else
					return "ignore";
			}
			else { // File does not exist in the client side
				System.out.println("THIS FILE" + serverFile+ "DOES NTO EXITS");
				return "Download";
			}
	}
	
	private int checkDates(String userDate,String serverDate) {
		
		//System.out.println("USER BEFORE CRASHING: " + userDate);
		Long usrDate = Long.parseLong(userDate);
		Long srvDate = Long.parseLong(serverDate);
		
		/*needs more analysis later*
		 *the server will upload download files only if 
		 *the difference between is one 30 sec or 1 minute 
		 */
		
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");

		System.out.println("USER DATE: " + usrDate + " " + sdf.format(new Date(usrDate)));
		System.out.println("SERVER DATE: " + srvDate + " " +sdf.format(new Date(srvDate)));
		
		if(srvDate > usrDate) {
			return 1;//download
		}
		else if(srvDate < usrDate) {
			return -1;//upload
		}
		else 
			System.out.println("EQUAL DATE");
			return 0;//Ignore 
		}
	
	private void prepareUserFiles() {
		System.out.println("----------------------");
		System.out.println("PREPARING FILES ");
		System.out.println("----------------------");
		int cnt = 0;
		int lastIndex;
		String tmp;
		
		path = "D:\\Eclipse Projects\\ITHWLearn\\client_files";
		File folder = new File(path);
		
		this.clientFiles = folder.listFiles();
		this.userFilesInStringArray = new String[clientFiles.length];
		this.userDateInStringArray = new String[clientFiles.length];
		this.userFiles = new ArrayList<String>();
		this.userfilesdates = new ArrayList<String>();
		while (cnt < clientFiles.length) {
			lastIndex = clientFiles[cnt].toString().lastIndexOf("\\");
			tmp = clientFiles[cnt].toString().substring(lastIndex + 1);
		
			this.userFilesInStringArray[cnt] = tmp;
			this.userFiles.add(tmp);
			//populate the userFilesDate Array with date
			this.userfilesdates.add(Long.toString(clientFiles[cnt].lastModified()));
			this.userDateInStringArray[cnt] = Long.toString(clientFiles[cnt].lastModified());
			cnt++;
			
		}//end of while
	}//end of prepare files method
	
	private void downloadFile(String fileToDownload) throws IOException, ParseException{
		
		String path = "D:\\Eclipse Projects\\ITHWLearn\\client_files\\"	+fileToDownload;
		FileOutputStream fileOutStream = new FileOutputStream(path);
		this.fileReader = new DataInputStream(in);
		
		System.out.println("DOWNLOADING...... "+ fileToDownload);
		
		/* =>Protocol
		 * send "Download" to the server
		 */
		this.Command = "Download";
		streamWriter.println(Command);
		//streamWriter.flush();
		
		this.Command = commandReader.readLine();
		strtok = new StringTokenizer(Command, " ");
		
		String ok = strtok.nextToken();
		int fileSize = Integer.parseInt(strtok.nextToken());
		byte[] bytes = new byte[fileSize];
		
		/*PROTOCOL*
		 *  
		 * SEND BACK "OK" => READY TO READ
		 */
		streamWriter.println("OK");
		
		fileReader.readFully(bytes);
		fileOutStream.write(bytes);
		fileOutStream.close();
		System.out.println("####\n Written \n#####");
		
		File file_with_new_date = new File(path);
		file_with_new_date.setLastModified(Long.parseLong(receivedLastModified));
		
		System.out.println("-----------------------------------------------------");
		System.out.println("DATE WAS : " + receivedLastModified);
		System.out.println("CHANGED DATE TO :" + file_with_new_date.lastModified());
		System.out.println("-----------------------------------------------------");		
		
		
	}
	
	private void uploadFile(String fileToUpload) throws IOException {
		
		System.out.println("UPLOADING...." + fileToUpload);
		String clientPath  = "D:\\Eclipse Projects\\ITHWLearn\\client_files" + "\\" + fileToUpload;
		lastDateModified = String.valueOf(new File(clientPath).lastModified());//Get the last date this file was modifed
		byte[] bytes;		
		fileInStream = new FileInputStream(clientPath);		
		this.fileWriter = new DataOutputStream(out);
		long fileSize = fileInStream.available();
		//need modification later, replace it by sending "upload fileSize"
		/* =>Protocol
		 * send upload to the server
		 * */
		System.out.println("DATE SENT TO SERVER: " + lastDateModified);
		Command = "upload "+ fileSize + " " + lastDateModified;//the last date this file was modified on the client side
		streamWriter.println(Command);
		
		/* =>Protocol
		 * send File Size to the server
		 * */
		bytes = new byte[(int) fileSize];
		fileInStream.read(bytes);
		String ok = commandReader.readLine();
		if(ok.equalsIgnoreCase("ok")) {
			System.out.println("PERMISSION TO WRITE BYTES TO THE STREAM");
			//return;
		}
		
		fileWriter.write(bytes);
		System.out.println("FINISHED UPLOADING: "+ fileToUpload);
		
//		Command = commandReader.readLine();
//		if(Command.equalsIgnoreCase("ok"))
//			System.out.println("FILE UPLOADED TO GOOGLE");
	}
	
}
